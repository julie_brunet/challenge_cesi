### Requetator 

## Equipe :  

Jeremie Aubert,   
Maxime Reutin,   
Gaëtan Gicquel,   
Christelle Moutarde,   
Julie Brunet,   

## Projet : 

Sniffeur de réseau (adresse IP / Mac) avec application mobile dédié.   
  

[Serveur Web2pi](https://bitbucket.org/julie_brunet/challenge_cesi/src/6fdc5992b9ce8a0f91fe4c55ff4bd29139a5cbb7/web2py/?at=master)  
[Configuration réseau](https://bitbucket.org/julie_brunet/challenge_cesi/src)  
[Application mobile cordova ](https://bitbucket.org/julie_brunet/challenge_cesi/src/d2f6004d2a4247f756634ece89e4394a05c236ca/app/?at=master)
  
Système supporter : IOS, Android, Browser