var app = ons.bootstrap('app', ['onsen']);

app.controller('AppController', function($scope, $http) {

    $(document.body).on("pageinit", '#page', function() {
        modal.show();
    });

    $http({
        method: 'GET',
        url: 'http://192.168.12.5:8000/scanner/main/map_addresses'
    }).then(function(response) {
        $scope.time = response.data[0].generationTime;
        $scope.addresses = response.data[0].addresses;
        modal.hide();
    });

    $scope.getMac = function(ip_address) {
        $scope.ip_address = ip_address;
    }

    $scope.arpPoisonning = function(ip_address) {
        ons.notification.confirm({
            title: "Requête ARP Poisonning",
            message: "Êtes vous sûr de vouloir continuer ?",
            callback: function(choix) {
                switch(choix) {
                    case 0:
                        break;
                    case 1:
                        analyse.show();
                        $http({
                            method: 'POST',
                            url: 'http://192.168.12.5:8000/scanner/main/map_addresses',
                            data: "array=" + JSON.stringify({ ip: ip_address, action: "ARP" }),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function(response) {
                            analyse.hide();
                            console.log(response);
                        });
                        break;
                }    
            }
        });    
    }

    $scope.ping = function(ip_address) {
        ons.notification.confirm({
            title: "Requête ping",
            message: "Êtes vous sûr de vouloir continuer ?",
            callback: function(choix) {
                switch(choix) {
                    case 0:
                        break;
                    case 1:
                        analyse.show();
                        $http({
                            method: 'POST',
                            url: 'http://192.168.12.5:8000/scanner/main/map_addresses',
                            data: "array=" + JSON.stringify({ ip: ip_address, action: "PING" }),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function(response) {
                            analyse.hide();
                            if (response.data == 'true') {
                                ons.notification.alert({
                                   message: "Le périphérique est disponible." 
                                });    
                            } else {
                                ons.notification.alert({
                                   message: "Le périphérique est indisponible." 
                                });    
                            }
                        });
                        break;
                }
            }
        });
    }

});
