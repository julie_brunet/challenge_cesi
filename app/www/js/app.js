var app = ons.bootstrap('app', ['onsen']);

app.controller('AppController', function($scope, $http) {

    $(document.body).on("pageinit", '#page', function() {
        modal.show();
    });

    $http({
        method: 'GET',
        url: 'http://192.168.0.20:8000/scanner/main/map_addresses'
    }).then(function(response) {
        $scope.time = response.data[0].generationTime;
        $scope.addresses = response.data[0].addresses;
        modal.hide();
    });

    $scope.getIp = function(ip_address) {
        $scope.ip_address = ip_address;
    }

    $scope.getMac= function(mac_address) {
        $scope.mac_address = mac_address;    
    }

    $scope.arpPoisonning = function(ip_address, mac_address) {
        ons.notification.confirm({
            title: "Requête ARP Poisonning",
            message: "Êtes vous sûr de vouloir continuer ?",
            callback: function(choix) {
                switch(choix) {
                    case 0:
                        break;
                    case 1:
                        analyse.show();
                        $http({
                            method: 'POST',
                            url: 'http://192.168.0.20:8000/scanner/main/map_addresses',
                            data: "array=" + JSON.stringify({ ip: ip_address, mac: mac_address, action: "ARP" }),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function(response) {
                            analyse.hide();
                            if (response.data == 'true') {
                                ons.notification.alert({
                                   message: "Le cache ARP a été empoisonné." 
                                });    
                            } else {
                                ons.notification.alert({
                                   message: "Impossible d'empoisonner le cache ARP." 
                                });    
                            }
                        });
                        break;
                }    
            }
        });    
    }

    $scope.ping = function(ip_address, mac_address) {
        ons.notification.confirm({
            title: "Requête ping",
            message: "Êtes vous sûr de vouloir continuer ?",
            callback: function(choix) {
                switch(choix) {
                    case 0:
                        break;
                    case 1:
                        analyse.show();
                        $http({
                            method: 'POST',
                            url: 'http://192.168.0.20:8000/scanner/main/map_addresses',
                            data: "array=" + JSON.stringify({ ip: ip_address, action: "PING" }),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function(response) {
                            analyse.hide();
                            if (response.data == 'true') {
                                ons.notification.alert({
                                   message: "Le périphérique est disponible." 
                                });    
                            } else {
                                ons.notification.alert({
                                   message: "Le périphérique est indisponible." 
                                });    
                            }
                        });
                        break;
                }
            }
        });
    }

});
