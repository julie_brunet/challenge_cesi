# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------
import socket, json, re

def index():
    """ 
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Ceci est le flash de web2py")
    return dict(message=T('Coucou et bienvenue sur le Requetator!'))

@cache.action()


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return ""


def valorize_lists():
	#  0 = Nouvelles addresses, 1 =  vieilles addresses
	files = ["tcp_dump.txt", "tcp_dump.txt.old"]
	mac_addresses = []
	try:
		for file in files:
			addresses = []
			with open("/srv/challenge/addresses/"+file) as f: 
				for line in f:	
					if re.search( '([0-9a-fA-F]:?){12}', line) and not line.strip() in mac_addresses:
						addresses.append(line.strip())
			mac_addresses.append(addresses)
	except Exception as e:
		return e
	return mac_addresses

@request.restful()
def map_addresses():
	def GET(*args, **vars):
		try:
			output = "" #Sortie
			timedate = "" #TimeStamp
			addresses = [] #Dict mac:ip
			tcp_dumps = valorize_lists()
			inactive_terminals = list(set(tcp_dumps[1]) - set(tcp_dumps[0]))
			newly_active_terminals = list(set(tcp_dumps[0]) - set(tcp_dumps[1]))
	
			with open("/srv/challenge/addresses/tcp_dump.txt") as f:
    				lines = f.readlines()
    				timedate = lines[-1].strip()
	
			arp_file = open('/srv/challenge/addresses/arp.txt', 'r')
			jsn = {}
			for line in arp_file:
				mac_address = line[line.find("t ")+2:line.find(" [ether] on eth0")]
				ip_address =  line[line.find("(")+1:line.find(")")]
			
				if mac_address in tcp_dumps[0]:
					status = "active"
				elif mac_address == " on eth0":
					status = "incomplete"
				else:
					status="inactive"

				line_address = {"mac_address": mac_address, "ip_address": ip_address, "status": status }
				addresses.append(line_address)

			jsn["addresses"] = addresses
			jsn["generationTime"] = timedate
			output = json.dumps([jsn])

		except Exception as e:
			return e
	
		return output
	
	def POST(*args, **vars):
		try:
			retour = "Ceci est un POST : "+str(args)
		except Exception as e:
			return e
		return retour
	def PUT(*args, **vars):
		return "Un put"
	def DELETE(*args, **vars):
		return "Un delete"

	return locals()

@request.restful()
def call_host():

    def GET(*args, **vars):
        return "Un futur JSON"

    def POST(*args, **vars):
        return "Pourquoi tu essayes de faire un post sur cette page? POURQUOOOOOIIII?\n Tes paramètres étaient sûrement"+request.vars

    def PUT(*args, **vars):
        return dict()

    def DELETE(*args, **vars):
        return dict()

    return locals()

